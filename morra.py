############# MORRA ##############
######## an experimental #########
### traditional high tech game ###
########### by bast1 #############
########### GPL v 3.0 ############
##################################
import random
import time
import sys


class Player(object):
    def __init__(self, name, lives):
        self.name = name
        self.lives = lives
        self.alive = True

    def loselife(self):
        self.lives -= 1
        if self.lives <= 0:
            self.alive = False
        return self.lives


class Game(object):
    def __init__(self):
        name = raw_input("'Bella mi 'fra, come hai detto che ti chiamavi?' ")
        lives = self._validate("'{0} t'a sient? a quanto ce la giochiamo?' ".format(name), (1, 100))
        self.difficulty = self._validate("'Da uno a cinque quanto fai brutto?' ", (1, 6))
        self.playerone = Player(name, lives)
        self.playertwo = Player("Rocc 'u macatron", lives)
        self.stillplaying = True
        self.playgame()
        self._continuegame()

    def playgame(self):
        while self.stillplaying is True:
            self.stillplaying = self._thehand()
        if self.playertwo.alive is False:
            print ("\nUaglio' hai fatto brutto.\nYOU WIN")
        elif self.playerone.alive is False:
            print ("\nT'agg dat""\nYOU LOSE")
        return

    def _continuegame(self):
        continue_choise = ("\nCosa vuoi fare ora?"
                           "\n1) Gioca ancora"
                           "\n2) Esci")
        print(continue_choise)
        decision = self._validate('inserisci la tua scelta:', (1, 3))
        if decision is 1:
            Game()
        else:
            print "\nCiao Uaglio'"
            return

    def _play(self, pgthrow, pgchoice):
        p2_lose_throw = pgchoice - pgthrow
        p1_could_win = True if p2_lose_throw <= 5 else False

        result = self._whoisthewinner(self.difficulty, p1_could_win)
        if result is 1:
            # 1) Vittoria del PG
            pngthrow = pgchoice - pgthrow
            top_limit = (pngthrow+6) if (pngthrow+6) < 11 else 11
            choiset_p2_lose = [x for x in range(2, top_limit) if x is not pgchoice]
            if len(choiset_p2_lose) is 0:
                choiset_p2_lose = [x for x in range(2, 11) if x is not pgchoice]
            pngchoice = random.SystemRandom().choice(choiset_p2_lose)
        elif result is 2:
            # 2) Vittoria del PNG
            top_limit = (pgthrow+5) if (pgthrow+5) < 11 else 11
            choiset_p2_win = [x for x in range(pgthrow + 1, top_limit) if x is not p2_lose_throw]
            pngchoice = random.SystemRandom().choice(choiset_p2_win)
            pngthrow = pngchoice - pgthrow
        else:
            # 0) pareggio
            choiset_p2_draw = [x for x in range(2, 11) if x is not pgchoice]
            pngchoice = random.SystemRandom().choice(choiset_p2_draw)
            p2_win_throw = pngchoice-pgthrow
            bottom_limit = (pngchoice-5) if pngchoice > 5 else 1
            top_limit = (pngchoice+1) if (pngchoice+1) < 6 else 6
            throwset_draw = [x for x in range(bottom_limit, top_limit) if x not in (p2_lose_throw, p2_win_throw)]
            if len(throwset_draw) is 0:
                throwset_draw = [x for x in range(1, 6) if x not in (p2_lose_throw, p2_win_throw)]
            pngthrow = random.SystemRandom().choice(throwset_draw)
        return result, pngthrow, pngchoice

    def _thehand(self):
        playerone = self.playerone
        playertwo = self.playertwo

        N2Luc = {
            2: "Uno p'd uno"
            , 3: "Trrrrrr"
            , 4: "Quatt' alla morra"
            , 5: "Cing' Cing'"
            , 6: "Sei alla morra"
            , 7: "Sett e vattenn'"
            , 8: "I gamb' d'u ragn"
            , 9: "Nov' Banom'"
            , 10: "Tutta quanda"
        }
        outcomes = [[None, "A pari si gioca"],
                    [playertwo, "M'hai fatto"],
                    [playerone, "T'ho fatt"]]

        pgthrow = self._validate("\nQuand' ne meni? ", (1,6))
        pgchoice = self._validate("Che allucchi? ", (2,11))
        result, pngthrow, pngchoice  = self._play(pgthrow, pgchoice)
        print("\n{} a terra!"
              "\nTu meni {} e allucchi {} ({})"
              "\nRocco mena {} e allucca {} ({})".format(
                    (pgthrow+pngthrow),
                    pgthrow,
                    N2Luc[pgchoice],
                    pgchoice,
                    pngthrow,
                    N2Luc[pngchoice],
                    pngchoice))
        loser, sentence = outcomes[result]
        if loser:
            loser.loselife()
        print (sentence)
        return False if playerone.alive is False or playertwo.alive is False else True

    @staticmethod
    def _whoisthewinner(difficulty, cw):
        if random.randint(1, int(1.0 / difficulty * 10)) == 1:
            result = 2
        elif cw and random.randint(1, difficulty * 2) == 1:
            result = 1
        else:
            result = 0
        return result

    @staticmethod
    def _validate(quest, hrange):
        while True:
            try:
                x = int(raw_input(quest))
                if x in range(hrange[0], hrange[1]):
                    return x
                print ("\nDevi utilizzare un numero tra {} e {}!".format(hrange[0], hrange[1] - 1))
            except ValueError:
                print ("\nDevi utilizzare un numero tra {} e {}!".format(hrange[0], hrange[1] - 1))
                pass


if __name__ == '__main__':
    Game()
    sys.exit()


